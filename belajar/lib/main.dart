//import 'ui_component/Login.dart';
import 'package:belajar/color/constant.dart';
import 'package:belajar/ui_component/login.dart';
import 'package:flutter/material.dart';
//import 'ui_component/Home.dart';



void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // final routes = <String, WidgetBuilder>{
  //   Login.tag:(context)=>Login(),
  //   LauncherPage.tag:(context)=>LauncherPage(),
  // };

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Gojek',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        fontFamily: 'NeoSans',
        primaryColor: GojekPalette.green,
        accentColor: GojekPalette.green,
      ),
      home: Login(),
      // routes: routes,
     
    );
  }
}

  

